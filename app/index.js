import React from "react";
import NavigationStack from "@navigation/navigationStack";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { NavigationContainer } from '@react-navigation/native';


const App = () => {

  return (
    <NavigationContainer>
        <NavigationStack/>
    </NavigationContainer>
  );
};

export default App;
